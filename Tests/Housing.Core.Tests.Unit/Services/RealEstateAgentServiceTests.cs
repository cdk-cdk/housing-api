﻿using FluentAssertions;
using Housing.Core.Interfaces;
using Housing.Core.Models;
using Housing.Core.Services;
using Housing.Tests.Data;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Housing.Core.Tests.Unit.Services
{
    public class RealEstateAgentServiceTests
    {
        private readonly Mock<IListingService> _listingServiceMock;

        public RealEstateAgentServiceTests()
        {
            _listingServiceMock = new Mock<IListingService>();
        }

        [Fact]
        public async Task CallingGetTop10Async_ReturnsExpectedRealEstateAgents()
        {
            // Arrange
            var allListings = new List<Listing>
            {
                new Listing(id: Guid.NewGuid().ToString(), makelaarId: "id1", makelaarNaam: "Makelaar Remax"),
                new Listing(id: Guid.NewGuid().ToString(), makelaarId: "id2", makelaarNaam: "House seller"),
                new Listing(id: Guid.NewGuid().ToString(), makelaarId: "id3", makelaarNaam: "House dj")
            };

            var expectedResult = new List<RealEstateAgent>
            {
                new RealEstateAgent(makelaarId: "id1", makelaarNaam: "Makelaar Remax"),
                new RealEstateAgent(makelaarId: "id2", makelaarNaam: "House seller"),
                new RealEstateAgent(makelaarId: "id3", makelaarNaam: "House dj")
            };

            _listingServiceMock.Setup(x => x.GetAllListingsAsync(It.IsAny<string>())).ReturnsAsync(() => allListings);
            var sut = new RealEstateAgentService(_listingServiceMock.Object);

            // Act
            var result = await sut.GetTop10Async("amsterdam");

            // Assert
            _listingServiceMock.Verify(x => x.GetAllListingsAsync("amsterdam"), Times.Once());
            result.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task CallingGetTop10Async_WithMoreThan10Listings_ReturnsAMaximumOf10Makelaars()
        {
            // Arrange
            var allListings = DomainModelsGenerator.ListingGenerator().Generate(200);

            _listingServiceMock.Setup(x => x.GetAllListingsAsync(It.IsAny<string>())).ReturnsAsync(() => allListings);
            var sut = new RealEstateAgentService(_listingServiceMock.Object);

            // Act
            var result = await sut.GetTop10Async("amsterdam");

            // Assert
            _listingServiceMock.Verify(x => x.GetAllListingsAsync("amsterdam"), Times.Once());
            result.Should().HaveCount(10);
        }
    }
}
