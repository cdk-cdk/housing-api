﻿using Bogus;
using Housing.Core.Models;

namespace Housing.Tests.Data
{
    public static class DomainModelsGenerator
    {
        /// <summary>
        /// Returns a faker object which allows the creation of Listings
        /// </summary>
        /// <returns>Listing generator</returns>
        public static Faker<Listing> ListingGenerator()
        {
            return new Faker<Listing>()
                        .StrictMode(true)
                        .RuleFor(o => o.Id, f => Guid.NewGuid().ToString())
                        .RuleFor(o => o.MakelaarId, f => Guid.NewGuid().ToString())
                        .RuleFor(o => o.MakelaarNaam, f => f.Company.CompanyName());
        }
    }
}