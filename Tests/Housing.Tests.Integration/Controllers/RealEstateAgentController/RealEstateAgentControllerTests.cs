﻿using FluentAssertions;
using Housing.Controllers.RealEstateAgentController.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Housing.Tests.Integration.Controllers.RealEstateAgentController
{
    public class RealEstateAgentControllerTests : WebApplicationFactory<Program>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public RealEstateAgentControllerTests()
        {
            _factory = new WebApplicationFactory<Program>();
        }

        [Fact]
        public async Task WhenCallingCalculateInsurances_WithValidInsurance_ReturnsCorrectInsuranceCost()
        {
            // Arrange
            var client = _factory.CreateClient();
            const string requestUri = "realestateagents/amsterdam%2Ftuin";

            // Act
            var response = await client.GetAsync(requestUri);
            var getAllRealEstateAgentsResponse =  JsonSerializer.Deserialize<GetAllRealEstateAgentsResponse>(await response.Content.ReadAsStringAsync(), new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            getAllRealEstateAgentsResponse.RealEstateAgents.Should().HaveCount(10);
        }
    }
}
