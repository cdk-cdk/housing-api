﻿using Housing.Core.Models;

namespace Housing.Core.Comparers
{
    public sealed class RealEstateAgentComparer : IEqualityComparer<RealEstateAgent>
    {
        public bool Equals(RealEstateAgent obj1, RealEstateAgent obj2) { return obj1.MakelaarId == obj2.MakelaarId; }

        public int GetHashCode(RealEstateAgent obj) { return obj.MakelaarId.GetHashCode(); }
    }
}
