﻿namespace Housing.Core.Models
{
    public sealed class RealEstateAgent
    {
        public string MakelaarId { get; set; }
        public string MakelaarNaam { get; set; }

        public RealEstateAgent()
        {
            
        }

        public RealEstateAgent(string makelaarId, string makelaarNaam)
        {
            MakelaarId = makelaarId;
            MakelaarNaam = makelaarNaam;
        }
    }
}
