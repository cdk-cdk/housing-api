﻿namespace Housing.Core.Models
{
    public class Listing
    {
        public string Id { get; set; }
        public string MakelaarId { get; set; }
        public string MakelaarNaam { get; set; }

        public Listing()
        {
            
        }

        public Listing(string id, string makelaarId, string makelaarNaam)
        {
            Id = id;
            MakelaarId = makelaarId;
            MakelaarNaam = makelaarNaam;
        }
    }
}
