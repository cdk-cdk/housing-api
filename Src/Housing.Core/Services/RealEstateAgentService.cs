﻿using Housing.Core.Comparers;
using Housing.Core.Interfaces;
using Housing.Core.Models;
using System.Collections.Concurrent;

namespace Housing.Core.Services
{
    public class RealEstateAgentService : IRealEstateAgentService
    {
        private readonly ConcurrentDictionary<RealEstateAgent, int> _realEstateAgents;
        private readonly IListingService _listingService;

        public RealEstateAgentService(IListingService listingService)
        {
            _listingService = listingService;
            _realEstateAgents = new(new RealEstateAgentComparer());
        }

        public async Task<IEnumerable<RealEstateAgent>> GetTop10Async(string query)
        {
            var listings = await _listingService.GetAllListingsAsync(query);

            foreach (var listing in listings)
            {
                var agent = new RealEstateAgent(listing.MakelaarId, listing.MakelaarNaam);
                // Add the real estate agent to the dictionary or increment his counter
                _realEstateAgents.AddOrUpdate(agent, 1, (key, oldValue) => _realEstateAgents[agent]+1);
            }

            return _realEstateAgents.OrderByDescending(kvp => kvp.Value)
                                  .Take(10)
                                  .Select(kvp => kvp.Key)
                                  .ToList();
        }
    }
}
