﻿using Housing.Core.Models;

namespace Housing.Core.Interfaces
{
    public interface IRealEstateAgentService
    {
        /// <summary>
        /// Gets all listings with a maximum of 10
        /// </summary>
        /// <param name="query">Search query</param>
        /// <returns>Top 10 real estate agents</returns>
        Task<IEnumerable<RealEstateAgent>> GetTop10Async(string query);
    }
}