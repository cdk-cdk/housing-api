﻿using Housing.Core.Models;

namespace Housing.Core.Interfaces
{
    public interface IListingService
    {
        /// <summary>
        /// Gets all listings
        /// </summary>
        /// <returns>All listings</returns>
        Task<IEnumerable<Listing>> GetAllListingsAsync(string query);
    }
}
