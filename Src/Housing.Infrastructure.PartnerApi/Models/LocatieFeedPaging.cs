﻿namespace Housing.Infrastructure.PartnerApi.Models
{
    [Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/FundaAPI.Feeds.Entities")]
    public partial class LocatieFeedPaging
    {
        public int AantalPaginas { get; set; }

        public int HuidigePagina { get; set; }
    }
}
