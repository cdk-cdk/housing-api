﻿namespace Housing.Infrastructure.PartnerApi.Models
{
    [Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/FundaAPI.Feeds.Entities")]
    public class LocatieFeedObject
    {
        public string Id { get; set; }

        public string MakelaarId { get; set; }

        public string MakelaarNaam { get; set; }
    }
}
