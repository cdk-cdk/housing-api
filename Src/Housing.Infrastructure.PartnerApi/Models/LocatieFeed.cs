﻿namespace Housing.Infrastructure.PartnerApi.Models
{

    [Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/FundaAPI.Feeds.Entities")]
    [System.Xml.Serialization.XmlRoot(Namespace = "http://schemas.datacontract.org/2004/07/FundaAPI.Feeds.Entities", IsNullable = false)]
    public partial class LocatieFeed
    {
        [System.Xml.Serialization.XmlArrayItem("Object", IsNullable = false)]
        public LocatieFeedObject[] Objects { get; set; }

        public LocatieFeedPaging Paging { get; set; }

        public ushort TotaalAantalObjecten { get; set; }
    }
}
