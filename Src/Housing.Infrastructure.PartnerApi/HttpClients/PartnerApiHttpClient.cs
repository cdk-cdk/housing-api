﻿using Housing.Infrastructure.PartnerApi.Models;
using Housing.Infrastructure.PartnerApi.Settings;
using Microsoft.Extensions.Options;
using System.Xml;
using System.Xml.Serialization;

namespace Housing.Infrastructure.PartnerApi.HttpClients
{
    public sealed class PartnerApiHttpClient : IPartnerApiHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly PartnerApiSettings _partnerApiSettings;

        public PartnerApiHttpClient(HttpClient httpClient, IOptions<PartnerApiSettings> partnerApiSettings)
        {
            _httpClient = httpClient;
            _partnerApiSettings = partnerApiSettings.Value;
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.BaseAddress = new Uri(_partnerApiSettings.BaseUrl);
        }

        public async Task<LocatieFeed?> GetListings(string query, int page, int pagesize)
        {
            var response = await _httpClient.GetAsync($"/feeds/Aanbod.svc/{_partnerApiSettings.ApiKey}/?type=koop&zo=/{query}&page={page}&pagesize={pagesize}");

            try
            {
                // Ensure the response is successful
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                ex.Data.Add("UserMessage", "An error occured while trying to fetch Aanbod.svc endpoint.");
                throw;
            }

            // Read the XML content from the response
            var xmlContent = await response.Content.ReadAsStringAsync();

            // Create an XmlSerializer instance for the Person class
            var serializer = new XmlSerializer(typeof(LocatieFeed));

            // Create a StringReader to read the XML content
            using var stringReader = new StringReader(xmlContent);

            // Create an XmlReader using the StringReader
            using var xmlReader = XmlReader.Create(stringReader);

            // Deserialize the XML into a Person object
            return serializer.Deserialize(xmlReader) as LocatieFeed;
        }
    }
}
