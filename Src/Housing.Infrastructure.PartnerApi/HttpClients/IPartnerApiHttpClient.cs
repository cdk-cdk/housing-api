﻿using Housing.Infrastructure.PartnerApi.Models;

namespace Housing.Infrastructure.PartnerApi.HttpClients
{
    public interface IPartnerApiHttpClient
    {
        Task<LocatieFeed?> GetListings(string query, int page, int pagesize);
    }
}
