﻿using Housing.Core.Interfaces;
using Housing.Core.Models;
using Housing.Infrastructure.PartnerApi.HttpClients;
using Housing.Infrastructure.PartnerApi.Mappers;
using Housing.Infrastructure.PartnerApi.Models;
using Microsoft.Extensions.Caching.Memory;

namespace Housing.Infrastructure.PartnerApi.Services
{
    public sealed class PartnerApiService : IListingService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly TimeSpan CacheLifeTime = TimeSpan.FromMinutes(30);

        /// <summary>
        /// Partner API has a 25 page size limitation 
        /// </summary>
        private readonly int PageSize = 25;
        private readonly IPartnerApiHttpClient _partnerApiHttpClient;

        public PartnerApiService(IPartnerApiHttpClient partnerApiHttpClient, IMemoryCache memoryCache)
        {
            _partnerApiHttpClient = partnerApiHttpClient;
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<Listing>> GetAllListingsAsync(string query)
        {
            // Checks if the query is already cached
            if (_memoryCache.TryGetValue(query, out List<Listing> cache))
            {
                return cache;
            }

            var page = 0;
            var resultList = new List<Listing>();
            LocatieFeed? result;
            do
            {
                page++;
                result = await _partnerApiHttpClient.GetListings(query, page, PageSize);

                if (result == null)
                {
                    throw new Exception("Error while retrieving the listings.");
                }

                resultList.AddRange(result.Objects.Select(item => item.ToListingCore()));

                // We can send only 100 queries per minute so we wait one minute every 90 queries
                if (page % 90 == 0)
                {
                    Thread.Sleep(60 * 1000);
                }
            }
            while (page * PageSize < result.TotaalAantalObjecten && result.Objects.Any());

            // Sets the query in the cache
            _memoryCache.Set(query, resultList, CacheLifeTime);

            return resultList;
        }
    }
}
