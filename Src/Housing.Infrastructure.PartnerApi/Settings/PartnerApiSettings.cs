﻿namespace Housing.Infrastructure.PartnerApi.Settings
{
    public sealed class PartnerApiSettings
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}
