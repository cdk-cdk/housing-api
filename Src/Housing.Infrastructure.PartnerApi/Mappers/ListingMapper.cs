﻿using Housing.Core.Models;
using Housing.Infrastructure.PartnerApi.Models;

namespace Housing.Infrastructure.PartnerApi.Mappers
{
    public static class ListingMapper
    {
        public static Listing ToListingCore(this LocatieFeedObject listingEntity)
        {
            return new Listing(listingEntity.Id, listingEntity.MakelaarId, listingEntity.MakelaarNaam);
        }
    }
}
