﻿using Housing.Infrastructure.PartnerApi.Settings;
using Microsoft.Extensions.Options;

namespace Housing.Validations
{
    public class PartnerApiSettingsValidation : IValidateOptions<PartnerApiSettings>
    {
        public ValidateOptionsResult Validate(string name, PartnerApiSettings options)
        {
            string? vor = null;
            if (string.IsNullOrWhiteSpace(options?.ApiKey))
            {
                vor += $"{nameof(PartnerApiSettings)}.ApiKey is not set. \n";
            }

            if (string.IsNullOrWhiteSpace(options?.BaseUrl))
            {
                vor += $"{nameof(PartnerApiSettings)}.BaseUrl is not set.";
            }

            if (vor != null)
            {
                return ValidateOptionsResult.Fail(vor);
            }

            return ValidateOptionsResult.Success;
        }
    }
}
