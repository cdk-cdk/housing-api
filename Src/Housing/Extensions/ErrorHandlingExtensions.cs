﻿using Microsoft.AspNetCore.Diagnostics;
using System.Diagnostics;

namespace Housing.Extensions
{
    public static class ErrorHandlingExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var logger = context.RequestServices.GetService<ILogger<Program>>();

                var exceptionDetails = context.Features.Get<IExceptionHandlerFeature>();
                var exception = exceptionDetails?.Error;

                logger.LogError(
                    exception,
                    "Could not process the request. TraceId: {TraceId}. Exception message: {Message}", Activity.Current?.Id, exception.Message);

                await Results.Problem(
                    title: "An error occured while processing your request.",
                    statusCode: StatusCodes.Status500InternalServerError,
                    extensions: new Dictionary<string, object?>
                    {
                        {"traceId", Activity.Current?.Id}
                    }
                )
                .ExecuteAsync(context);
            });
        }
    }
}
