﻿using Housing.Controllers.RealEstateAgentController.Models;
using Housing.Core.Models;

namespace Housing.Controllers.RealEstateAgentController.Mappers
{
    public static class RealEstateAgentControllerMapper
    {
        public static GetAllRealEstateAgentsResponse ToGetAllRealEstateAgentsResponse(this IEnumerable<RealEstateAgent> realEstateAgents)
        {
            if (!realEstateAgents.Any()) return new GetAllRealEstateAgentsResponse();

            GetAllRealEstateAgentsResponse getAllRealEstateAgentsResponse = new ();

            foreach (var realEstateAgent in realEstateAgents)
            {
                getAllRealEstateAgentsResponse.RealEstateAgents.Add(realEstateAgent);
            }

            return getAllRealEstateAgentsResponse;
        }
    }
}
