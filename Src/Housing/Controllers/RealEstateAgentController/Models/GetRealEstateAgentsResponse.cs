﻿using Housing.Core.Models;

namespace Housing.Controllers.RealEstateAgentController.Models
{
    public class GetAllRealEstateAgentsResponse
    {
        public List<RealEstateAgent> RealEstateAgents { get; set; } = new();
    }
}
