﻿using Housing.Controllers.RealEstateAgentController.Mappers;
using Housing.Controllers.RealEstateAgentController.Models;
using Housing.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Housing.Controllers.RealEstateAgentController
{
    /// <summary>
    /// Provides access to the real estate agents
    /// </summary>
    [ApiController]
    [Route("realestateagents")]
    public class RealEstateAgentController : ControllerBase
    {
        private readonly ILogger<RealEstateAgentController> _logger;
        private readonly IRealEstateAgentService _realEstateAgentService;

        public RealEstateAgentController(ILogger<RealEstateAgentController> logger, IRealEstateAgentService realEstateAgentService)
        {
            _logger = logger;
            _realEstateAgentService = realEstateAgentService;
        }

        /// <summary>
        /// Get all real estate agents limited to a maximum of 10
        /// </summary>
        /// <param name="query">Search query, ex: amsterdam/tuin /param>
        /// <returns>List of real estate agents</returns>
        [HttpGet("{query}", Name = "GetTop10RealEstateAgents")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GetAllRealEstateAgentsResponse>> GetTop10RealEstateAgents(string query)
        {
            var realEstateAgents = await _realEstateAgentService.GetTop10Async(query);
            return Ok(realEstateAgents.ToGetAllRealEstateAgentsResponse());
        }
    }
}
