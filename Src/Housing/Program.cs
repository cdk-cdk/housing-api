using FluentValidation.AspNetCore;
using Housing.Core.Interfaces;
using Housing.Core.Services;
using Housing.Extensions;
using Housing.Infrastructure.PartnerApi.HttpClients;
using Housing.Infrastructure.PartnerApi.Services;
using Housing.Infrastructure.PartnerApi.Settings;
using Housing.Validations;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMemoryCache();

builder.Services.AddMvcCore()
    .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Program>());

builder.Services.AddScoped<IRealEstateAgentService, RealEstateAgentService>();

// PartnerAPI
builder.Services.Configure<PartnerApiSettings>(builder.Configuration.GetSection("PartnerApiSettings"));
builder.Services.AddSingleton<IValidateOptions<PartnerApiSettings>, PartnerApiSettingsValidation>();
builder.Services.AddHttpClient<IPartnerApiHttpClient, PartnerApiHttpClient>();
builder.Services.AddScoped<IListingService, PartnerApiService>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseExceptionHandler(exceptionHandlerApp => exceptionHandlerApp.ConfigureExceptionHandler());

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }