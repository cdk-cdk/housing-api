# Housing assignment

Provides the top 10 real estate agents which have the most object listed for sale. We fetch the data from an external API which lists housings for sale per city.

To complete this task, a one-page script in python would have done the job more quickly but in our context, I preferred to write a .NET API.

### Improvements
- To handle the 100 queries limitation from the external API, I implemented a sleep mechanism within each request. We can improve it by having a global mechanism (on every requests). To do so, we could use a Channel (Producer/Consomer pattern) with a capacity of 100 tokens, each request will need to have a token to be able to query the external API.

## Description

Project is based on C# .NET 6.  I followed a DDD design approach. I use IMemoryCache to cache the response from the external API.

This project is an one endpoint API. In order to allow an user to build the solution right away, I left on purpose the ApiKey in the appsettings.json. One option is to put the ApiKey in the user-secrets.

## Technologies
- .NET 6.0
- ASP.NET Core
- IMemoryCache

For testing purposes, four packages are necessary: [xUnit](https://xunit.net/), [FluentAssertions](https://www.nuget.org/packages/FluentAssertions/), [moq](https://www.nuget.org/packages/moq/) and [bogus](https://www.nuget.org/packages/Bogus).

## Installing / Getting Started

Clone using
```bash
git clone https://gitlab.com/cdk-cdk/housing-api.git
```

I recommend opening the solution with Visual Studio and build it with UI tools.
Otherwise, open a terminal and type

```powershell
cd .\Src\Housing\
dotnet restore .\Housing.csproj
dotnet run
```

### Verify deployment

Verify the deployment by navigating to the swagger address in your preferred browser.

```bash
http://localhost:5285/swagger/index.html
or
https://localhost:7285/swagger/index.html
```
## Query the endpoint

Use the swagger, postman or your browser
```bash
https://localhost:7285/realestateagents/amsterdam%2Ftuin
```

### Postman
In the Tests folder, there is a postman collection with 1 query to interact with the API (the ApiKey is already included).

## Testing
There are 3 tests in total, they shall all pass.